--создание
CREATE DATABASE library;
--DROP DATABASE library;

USE  library;

CREATE TABLE Users(
    id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
    birthday DATE NOT NULL,
    address TEXT,
    phone_number VARCHAR(12) UNIQUE NOT NULL,
    PRIMARY KEY(id)
);

ALTER TABLE Users ADD pass VARCHAR(32);
ALTER TABLE Users DROP pass;

CREATE TABLE Writers (
    id INT NOT NULL AUTO_INCREMENT,
    full_name VARCHAR(50) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE Books(
    id INT NOT NULL AUTO_INCREMENT,
    title VARCHAR(50),
    id_writer INT,
    year YEAR,
    number INT,
    FOREIGN KEY (id_writer) REFERENCES Writers (id),
    PRIMARY KEY(id)
);

CREATE TABLE Delivery(
    id INT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY(id),
    id_book INT,
    FOREIGN KEY (id_book) REFERENCES Books (id),
    date_delivery DATE NOT NULL,
    date_return DATE NOT NULL,
    id_user INT,
    FOREIGN KEY (id_user) REFERENCES Users (id)
);

CREATE INDEX N_ind ON Users (name);

DROP INDEX N_ind ON Users;

INSERT INTO `Writers`(full_name) 
VALUES ('Булгаков MA'),
('Пушкин АС'),
('Достоевский ФМ'),
('Толстой ЛН'),
('Оруэлл Дж'),
('Ли Харпер'),
('Ремарк Эрих Мария'),
('Сент-Экзюпери Антуан де'),
('Лермонтов МЮ'),
('Чехов АП');

INSERT INTO `Books`(title, year, id_writer, number) 
VALUES ('Мастер и Маргарита', '2016', 1, '2'),
('Евгений Онегин', '2020', 2, '5'),
('Преступление и наказание', '1990', 3, '1'),
('Война и мир', '1980', 4, '4'),
('1984', '2017', 5, '1'),
('Убить пересмешника', '2021', 6, '3'),
('Триумфальная арка', '2011', 7, '5'),
('Маленький принц', '2020', 8, '4'),
('Герой нашего времени', '1999', 9, '8'),
('Вишневый сад', '2005', 10, '6');

INSERT INTO Users (name, birthday, address, phone_number)
VALUES
('Иванов ВБ', 2002-12-21, 'Гороховая улица, 44Б', '7999888777777'),
('Кузнецова МН', 1980-06-24, NULL,'7911222333333'),
('Белова АП', 1962-04-10, NULL, NULL),
('Петров МТ', 1996-08-16, 'переулок Гривцова, 5В', NULL),
('Трофимов ГД', 1975-01-27, NULL, NULL);

INSERT INTO Users (name, birthday, address, phone_number)
VALUES
('Баранов ПП', '1933-02-07', 'Гороховая улица, 15', '7999333666666');

UPDATE Users SET Users.birthday = '2002-12-21'
WHERE id = 1;

UPDATE Users SET Users.birthday = '1980-06-24'
WHERE id = 2;

UPDATE Users SET Users.birthday = '1962-04-10'
WHERE id = 3;

UPDATE Users SET Users.birthday = '1996-08-16', phone_number = '7923239232323'
WHERE id = 4;

UPDATE Users SET Users.birthday = '1975-01-27', address = 'Литейный проспект, 36'
WHERE id = 5;

INSERT INTO Delivery (id_book, date_delivery, date_return, id_user)
VALUES
('5', '2021-03-12', '2021-03-27', '1'),
('8', '2021-03-15', '2021-03-25', '5'),
('10', '2021-04-03', '2021-04-05', '2'),
('1', '2021-04-05', '2021-04-10', '4'),
('2', '2021-04-06', '2021-04-21', '6'),
('5', '2021-04-06', '2021-04-15', '2'),
('4', '2021-04-06', '2021-04-23', '3'),
('3', '2021-04-06', '2021-04-07', '3'),
('6', '2021-04-10', '2021-04-28', '1'),
('10', '2021-04-12', '2021-04-30', '5'),
('7', '2021-04-15', '2021-05-02', '3');



--Удаление
CREATE TABLE Test (
    id INT NOT NULL,
    Text TEXT,
    file VARCHAR(5),
    PRIMARY KEY (id)
);

INSERT INTO Test (id, Text, file)
VALUES (1, 'текст текст', 'тест'),
        (2, 'текст текст текст', 'тест'),
        (3, 'текст текст текст текст', 'тест'),
        (4, 'текст текст текст', 'тест'),
        (5, 'текст текст', 'тест'),
        (6, 'текст', 'тест');

DELETE FROM Test
WHERE id >4;

DELETE FROM Test;

INSERT INTO Test (id, Text, file)
VALUES (1, 'текст текст', 'тест'),
        (2, 'текст текст текст', 'тест'),
        (3, 'текст текст текст текст', 'тест'),
        (4, 'текст текст текст', 'тест'),
        (5, 'текст текст', 'тест'),
        (6, 'текст', 'тест');

TRUNCATE TABLE Test;

DROP TABLE Test;

--Выборка Where, Order, Limit
SELECT * FROM Users;

SELECT name, birthday, phone_number FROM Users;

SELECT id, name, birthday FROM Users
WHERE id >= 3 AND id < 6;

SELECT * FROM Users
WHERE id <> 5;

SELECT * FROM Users
WHERE id <> 5 AND address IS NOT NULL;

SELECT * FROM Users
WHERE name LIKE 'Б%' OR phone_number IS NULL;

SELECT * FROM Users
WHERE name LIKE '%П' XOR id >=3;

SELECT DISTINCT id_user FROM Delivery
ORDER BY id_user;

SELECT * FROM Books 
WHERE year IN ('2021', '1980', '2005');

SELECT * FROM Books
WHERE number BETWEEN 3 AND 6
ORDER BY year DESC;

SELECT * FROM Books
WHERE year LIKE '20__'
ORDER BY number;

SELECT * FROM Books
ORDER BY number DESC
LIMIT 2, 5;

SELECT * FROM Books
WHERE title LIKE '%на%'
ORDER BY id_writer;

--Объединение данных
SELECT Delivery.id_book, Delivery.id_user, Users.name, Users.phone_number
FROM Users
INNER JOIN Delivery ON Delivery.id_user = Users.id
ORDER BY Delivery.id_book; 

SELECT Writers.full_name, Books.title, Users.name, Delivery.date_delivery, Delivery.date_return
FROM Writers
INNER JOIN Books ON Writers.id = Books.id_writer
INNER JOIN Delivery ON Delivery.id_book = Books.id
INNER JOIN Users ON Users.id = Delivery.id_user
ORDER BY Delivery.date_delivery DESC;

SELECT Books.title, Books.year, Delivery.date_delivery
FROM Books
LEFT JOIN Delivery ON Delivery.id_book = Books.id
ORDER BY Books.title;

SELECT Books.title, Books.year, Delivery.date_delivery
FROM Books
RIGHT JOIN Delivery ON Delivery.id_book = Books.id
ORDER BY Books.title;

UPDATE Delivery
SET id_user = 2
WHERE id BETWEEN 4 AND 5;

SELECT Users.name, Books.title, Delivery.date_delivery, Delivery.date_return
FROM Delivery
LEFT JOIN Users ON Users.id = Delivery.id_user
LEFT JOIN Books ON Books.id = Delivery.id_book
ORDER BY Users.name;

--функции
SELECT name, CONCAT(address, ', номер: ', phone_number) AS 'Информация'
FROM Users;

SELECT COUNT(id)
FROM Delivery;

SELECT name, MIN(birthday)
FROM Users
GROUP BY birthday;

SELECT id_book, COUNT(id_book)
FROM Delivery
GROUP BY id_book;

SELECT id_book, id_user, COUNT(id_user) AS pc
FROM Delivery
GROUP BY id_user
HAVING pc > 2;
